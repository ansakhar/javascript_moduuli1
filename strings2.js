//alkuperäinen merkkijono
let str = process.argv[2];
console.log(str);

//alkuperäinen merkkijono
const args = process.argv.slice(2).join(" ");
console.log(args);

//Trimmaa merkkijonon molemmista päistä
str = str.trim();
console.log(str);

//Lyhentää sen maksimissaan 20 merkkiin
str = str.substring(0,20);
console.log(str);

/*const lowercase = str.toLowerCase();
console.log(lowercase);*/

//Muuttaa ensimmäisen kirjaimen pieneksi kirjaimeksi
str = str.substring(0,1).toLowerCase() + str.substring(1);
console.log(str);


