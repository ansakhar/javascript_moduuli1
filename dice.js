const diceType = process.argv[2];
if ((!diceType) || (diceType.substring(0,1) !== "D")) {
  console.log ("The argument must be provided as 'DN', where N = number.");
}
else {
  let sides = Number(diceType.substring(1));
  console.log(sides);
  if(!diceType.substring(1)) {  //if sides number is missing 
    console.log ("Enter the number of sides. The argument must be provided as 'DN', where N = number.");
  }
  else if (!Number.isInteger(sides)) { //in case of NaN or decimal
    console.log ("The number of sides should be an integer.");
  } 
  else if (sides > 100) {
    console.log ("The number of sides should be a number betwwen 3 and 100.");
  }
  else {
    if (sides < 3) { //If the user provides a number smaller than 3
      sides = 6;
      console.log("Default number of sides has been set as 6.");
    }
    //console.log(sides);
    const x = require("readline");
    const rl = x.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    const recursiveAsyncReadLine = function () {
      rl.question("Press <enter< for dice roll, type 'q' + <enter> to exit.", function (answer) {
        if (answer === "q")
        {
          console.log("Bye!");
          return rl.close(); //closing RL and returning from function.
        }
        const roll = Math.floor(Math.random() * sides) + 1;
        if (roll === 1) {
          console.log("Sad! The roll is", roll);
        }
        else if (roll === sides) {
          console.log("Happy! The roll is", roll);
        }
        else {
          console.log("The roll is", roll);
        }
        recursiveAsyncReadLine(); //Calling this function again to ask new question
      });
    };

    recursiveAsyncReadLine();
  }
}