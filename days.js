const month = parseFloat(process.argv[2]);

function daysInMonth (month, year) {
  return new Date(year, month, 0).getDate();
}

console.log(daysInMonth(month, 2022));