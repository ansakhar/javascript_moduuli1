let sum = 0;
const n = 17;
for(let i = 0; i <= n; i++) {
  if((i % 3) === 0 || (i % 5) === 0)
    sum += i;
}
console.log("for-loop", sum);

let i = 0;
sum = 0;
while(i <= n) {
  if((i % 3) === 0 || (i % 5) === 0) 
    sum += i;
  i++;
}
console.log("while-loop", sum);