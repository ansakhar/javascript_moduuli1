const balance = 100;
const isActive = true;
const checkBalance = true;

if (!checkBalance) console.log("Have a nice day!");
else if (!isActive) console.log("Your account is not active");
else if (balance > 0) console.log("Your balance is", balance);
else if ( balance === 0) console.log("Your account is empty");
else console.log("Your balance is negative.");