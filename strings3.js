/*Merkkijonossa ei ole tyhjiä merkkejä päissä
Ei ole yli 20 merkkiä pitkä
Ei ala isolla kirjaimella
*/

//alkuperäinen merkkijono
const str = process.argv[2];
console.log(str);

if((str.trim() === str) && (str.length < 20) && (str.substring(0,1).toLowerCase() === str.substring(0,1)))
{
  console.log("merkkijono vastaa ehtoja");
} 
else {
  console.log("merkkijono ei vastaa ehtoja");
}