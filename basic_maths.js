//Discounted price
const price = 100;
const discount = 20;
console.log("discounted price: ", price * (100 - discount) / 100);

//Travel time
const distance = 1000;
const speed = 100;
console.log("travel time: ", distance / speed);

//Seconds in a year
const days = 365;
const hours = 24;
const seconds = 3600;
const secondsYear = days * hours * seconds;
console.log("Seconds in a year: ", secondsYear);

//Area of square
const side = process.argv[2];
const area = side * side;
console.log("Area of square: ", area);