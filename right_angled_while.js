const n = 4;
let row = "";
let i = 1;
while (i <= n) {
  row = "";
  for(let j = 0; j < n - i; j++) {
    row += "*";
  }
  for (let s = 0; s < (i*2-1); s++) {
    row += "&";
  }
  for(let j = 0; j < n - i; j++) {
    row += "*";
  }
  console.log(row);
  i++;
}
