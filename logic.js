const playerCount = 4;
if (playerCount === 4) {
  console.log("You can play");
}

const isStressed = true;
const hasIcecream = true;
if ((!isStressed)||(hasIcecream)) {
  console.log("happy");
}

const sun = true;
const rain = false;
const temp = 20;
if ((sun)&&(!rain)&&(temp >= 20)) {
  console.log("It's a beach day");
}

const Suzy = true;
const Dan = false;
const day = "tuesday";
const time = "night";
if (((Suzy && !Dan)||(!Suzy && Dan)) && (day === "tuesday") && (time==="night")) {
  console.log("Arin is happy");
} else {
  console.log("Arin is sad");
}