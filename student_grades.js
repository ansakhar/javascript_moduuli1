const students = [
  {name: "markku", score: 99}, {name: "karoliina", score: 58},
  {name: "susanna", score: 69}, {name: "benjamin", score: 77},
  {name: "isak", score: 49}, {name: "liisa", score: 89},
];

//Find the highest, lowest scoring person.
let maxScore = students[0].score;
let minScore = students[0].score;
let maxInd = 0;
let minInd = 0;
for (let index = 0; index < students.length; index++) {
  if (students[index].score >= maxScore) {
    maxInd = index;
    maxScore = students[index].score;
  }
  if (students[index].score <= minScore) {
    minInd = index;
    minScore = students[index].score;
  }
}
console.log("the highest scoring person:", students[maxInd]);
console.log("the lowest scoring person:", students[minInd]);

//Then find the average score of the students.
let sum = 0;
students.forEach(element => {
  sum += element.score;
});
const average = sum / students.length;
console.log("average score: ", average);

//Then print out only the students who scored higher than the average.
students.forEach(element => {
  if (element.score > average) console.log(element);
});

//Assign grades 1-5 for each student based on their scores.
students.forEach(element => {
  switch(true) {
    case ((element.score >= 0) && (element.score  <= 39)):
      element.grade = "1"; 
      break;
    case ((element.score >= 40) && (element.score  <= 59)):
      element.grade = "2"; 
      break;
    case ((element.score >= 60) && (element.score  <= 79)):
      element.grade = "3"; 
      break;
    case ((element.score >= 80) && (element.score  <= 94)):
      element.grade = "4"; 
      break;
    case ((element.score >= 95) && (element.score  <= 100)):
      element.grade = "5"; 
      break;
  }
});

console.log(students);
